/*
 * In dit javascript bestand plaats je de code om met behulp van cleave.js formvalidatie toe te passen op de ticketForm.
 * Gebruik hiervoor de documentatie op https://nosir.github.io/cleave.js/ 
 */

//date
let cleaveDate = new Cleave('.dateField', {
    date: true,
    delimiter: '-',
    datePattern: ['d', 'm', 'Y']
});

// credit card
let cleaveCreditCard = new Cleave('.creditCardField', {
    creditCard: true
});

// phone number
let cleavePhoneNumber = new Cleave('.phoneNrField', {
    phone: true,
    prefix: '+32',
    phoneRegionCode: '{country}',
    numericOnly: true
});
