//Oefening 78 (extra): Functie die de prijs van het gekozen bestemming toont:
let ticketsForm = document.getElementById("ticketForm");

function showDestinationPrice(e) {
    e.preventDefault();
    let ticketsFormData = new FormData(ticketsForm);
    // De gekozen bestemming verkrijgen
    let selectedDestination = ticketsFormData.get("destination");
    // De prijs van het gekozen bestemming verkrijgen:
    let price;
    for (let i = 0; i < destinationArr.length; i++) {
        if (destinationArr[i].name == selectedDestination) {
            price = destinationArr[i].price;
        }
    }
    // Prijs tonen
    let elDestinationPrice = document.getElementById("destinationPrice");
    elDestinationPrice.textContent = `€${price}`;
}
document.getElementById("destinationSelect").addEventListener("change", showDestinationPrice);

//Oefening 79 (extra): Functie die alle bestelde tickets in een lijst onder de ticketForm plaatst

function AddTickets(e) {
    e.preventDefault();
    let ticketsFormData = new FormData(e.target);
    // De benodigde data verkrijgen en opslaan in variabelen
    let name = ticketsFormData.get("name");
    let destination = ticketsFormData.get("destination");
    let date = ticketsFormData.get("date");

    // De tekst tonen in een lijst onder de form
    let listParent = document.getElementById("listTickets");
    let listItem = document.createElement("li");
    listItem.textContent = `Bestelling: ${name}, ${destination} ${date}`;
    listParent.appendChild(listItem);
}
document.getElementById("ticketForm").addEventListener("submit", AddTickets);