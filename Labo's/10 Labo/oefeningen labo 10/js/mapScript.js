/* 
 * In dit script plaats je de code om de kaart in de zijbalk te tekenen.
 */

// map initialiseren coördinatenlaag
let myMap = L.map('stationMap', { // gebruik id "stationMap" in HTML
  center: [51.2301, 4.41774], // middelpunt van map
  zoom: 16 // schaal van de map
});

// map initialiseren kaartlaag
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	id: 'mapbox.streets',
  accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
}).addTo(myMap);

// duimspijker toevoegen
let markerBusStation = L.marker([51.2301, 4.41774]);
markerBusStation.bindPopup('<b>Busstation</b><br>Ellermanstraat');
markerBusStation.addTo(myMap);


// Rechthoek maken rond de adres
var bounds = [[51.23041, 4.41731], [51.22991, 4.41825]];
// create an orange rectangle
L.rectangle(bounds, {color: "#ff7800", weight: 1}).addTo(myMap);