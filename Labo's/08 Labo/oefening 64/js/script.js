// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Een variabele aanmaken waarin we het aantal keer dat er is geraden bijhouden
let guesses = 1;

// Een random-getal aanmaken
let number = Math.floor(Math.random() * 11);
// We vragen een getal aan de gebruiker
let numberGuessed = prompt("Geef een getal in");
// Zolang deze getal niet gelijk is aan het random-getal herhalen we de vraag met een hint
while (numberGuessed != number) {
    if (numberGuessed < number) {
        numberGuessed = prompt("Geef een getal hoger in");
    } else if (numberGuessed > number) {
        numberGuessed = prompt("Geef een getal lager in");
    }
    guesses++;
}
// Wanneer het juiste getal is geraden tonen we dit op het scherm
showText("Je hebt het juiste getal geraden!", "number");
showText("U hebt het getal in " + guesses + " keren geraden", "guesses");
