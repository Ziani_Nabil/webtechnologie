// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// De gebruiker vragen om een paswoord en deze opslaan in een variabele
let password = prompt("Geef een paswoord in");
// Als paswoord gelijk is aan Wireframe tonen we iets op het scherm en anders tonen we iets anders 
if (password === "Wireframe") {
    showText("Toegang toegestaan", "password");
} else {
    showText("Toegang geweigerd", "password");
}