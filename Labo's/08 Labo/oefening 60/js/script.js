// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
	let element = document.getElementById(id);
	element.textContent = text;
};

// De gebruiker vragen om zijn score en deze opslaan in een variabele
let score = prompt("Geef je score in");
// Als score groter/gelijk is aan 50 tonen we iets op scherm en anders tonen we iets anders
if (score >= 50) {
	showText("Geslaagd", "score");
} else {
	showText("Niet geslaagd", "score");
}
