// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Een random-getal aanmaken
let number = Math.floor(Math.random() * 11);
// We vragen een getal aan de gebruiker
let numberGuessed = prompt("Geef een getal in");
// Zolang deze getal niet gelijk is aan het random-getal blijven we deze vraag herhalen
while (numberGuessed != number) {
     numberGuessed = prompt("Geef een getal in");
}
// Wanneer het juiste getal is geraden tonen we dit op het scherm
showText("Je hebt het juiste getal geraden!", "number");
