// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// De gebruiker vragen om een bewerking en deze opslaan in een variabele
let bewerking = prompt("Geef een bewerking in");
// 2 getallen vragen en opslaan in variabele
let getal1 = prompt("Geef het eerste getal in: ");
let getal2 = prompt("Geef het tweede getal in: ");

let resultaat;
// Naargelang de keuze van bewerking doen we verschillende zaken
switch(bewerking) {
    case "-":
        resultaat = getal1 - getal2;
        break;
    case "+":
        resultaat = getal1 + getal2;
        break;
    case "*":
        resultaat = getal1 * getal2;
        break;
    case "/":
        resultaat = getal1 / getal2;
        break;
    default:
        resultaat = "Foutieve keuze";
       };
// Het resultaat op scherm tonen
showText(resultaat, "resultaat");