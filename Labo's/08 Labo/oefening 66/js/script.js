// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Een getal waarvan we het faculteit gaan berekenen aanmaken 
let number = 5;
let result = 1;

for (let i = 1; i <= number; i++) {
    result *= i;
}

showText(`Het faculteit van ${number} is ${result}`, "mainHeading");