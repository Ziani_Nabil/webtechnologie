// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

/* De getallen van 1 tot 100 printen in de console, bepaalde getallen worden vervangen door tekst a.d.h.v. bepaalde voorwaarden (deelbaar door..) */
for(let i = 1; i <= 100; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
        console .log('FizzBuzz');
    } else if (i % 3 == 0) {
        console.log('Fizz');        
    } else if (i % 5 == 0) {
        console.log('Buzz');
    } else {
        console.log(i);
    }
}