// 2 variabelen declareren en initialiseren
let userName = "Ziani Nabil";
let message = "Welcome!";

// Deze instructie slaat het html element op in de variabele.
let userName1 = document.getElementById("userName");
let message1 = document.getElementById("message");

// Deze instructie wijzigt de tekst van de element naar de gegeven waarde.
userName1.textContent = userName;
message1.textContent = message;