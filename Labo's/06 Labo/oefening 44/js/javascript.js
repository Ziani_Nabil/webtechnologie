// Array aanmaken met drie waardes
let colors = new Array("wit", "zwart", "rood");

// Het html-element element met id ="heading" opslaan in de variabele heading
let heading = document.getElementById("heading");

// We wijzigen de tekst van de html-element naar de waarde van het 2de element van colors.
heading.textContent = colors[1];
