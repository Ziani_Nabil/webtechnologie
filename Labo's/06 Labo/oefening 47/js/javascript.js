// Een variabele input aanmaken en een waarde geven
let input = 11;

/* Een andere variabele output aanmaken die de waarde 0 krijgt als de variabele input even is en de waarde 1 als de variabele oneven is. */
let output;

if (input % 2 === 0) {
output = 0;        
} else {
output = 1;    
}

// Element met id output wordt ingeladen.
let outputWaarde = document.getElementById("output");

// We plaatsen output in het h1-element
outputWaarde.textContent = output;
