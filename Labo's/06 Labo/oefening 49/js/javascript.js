// 2 variabelen aanmaken en waarde toekennen, de naam en de lengte ervan.
let name = "Nabil";
let lengthName = name.length;

// De prijs berekenen en in variabele prijs opslaan
let price = (5 * lengthName);

// De prijs op het scherm tonen
let mainHeading = document.getElementById("prijsNaamplaatje");
mainHeading.textContent = `Dag ${name}, je naamplaatje zal ${price} euro kosten`
