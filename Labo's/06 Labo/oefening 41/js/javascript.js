// Variabele declareren
let title;

// Waarde aan variabele toeschrijven
title = "Hello world!";

// Deze instructie slaat het html element met id="mainHeading" op in de variabele mainHeading.
let mainHeading = document.getElementById("mainHeading"); 

// Deze instructie wijzigt de tekst van het element in mainHeading naar de waarde die in title staat.
mainHeading.textContent = title; 