// 3 variabelen aanmaken om waarden in op te slagen.
// Price en Quantity krijgen waarde
let price = 5, quantity = 20, total;

// Verander price naar 3 euro
price = 3;

// De totaalprijs berekend en we slaan dit op in de variabele total.
total = price * quantity;

// Deze instructie slaat het html element met id="priceSpan" op in de variabele priceSpan.
let priceSpan = document.getElementById("priceSpan"); 

// Deze instructie wijzigt de tekst van the element in priceSpan naar de waarde die in total staat.
priceSpan.textContent = total; 

// Het h1 element wordt ingeladen.
let mainHeading = document.getElementById("mainHeading");

// We plaatsen een andere tekst in het h1 element.
mainHeading.textContent = "Demo oefening labo 6";

// alert toont een melding op het scherm van de gebruiker met daarin de meegegeven waarde.
alert(total);