// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// De template maken voor een object bus
function Bus(licensePlate, destination, seats, seatsReserved) {
    // De verschillende properties, waaronder een array passengersArray
    this.licensePlate = licensePlate;
    this.destination = destination;
    this.seats = seats;
    this.seatsReserved = seatsReserved;
    this.passengers = new Array();

    this.checkAvailability = function () {
        let seatsLeft = (seats - seatsReserved);
        return seatsLeft;
    };
}

// Constructor aanmaken voor passagier met behorende properties
 function Passenger(name, nationality, age) {
    this.name = name;
    this.nationality = nationality;
    this.age = age;
}

// Een bus aanmaken met daarin 2 passagiers
let bus = new Bus('1-ABC-123', 'Oostenrijk', 75, 62);
bus.passengers[0] = new Passenger('Nabil', 'Belg', 21);
bus.passengers[1] = new Passenger('Ilias', 'Nederlander', 45);

// Tekst tonen in html-document
showText(bus.destination, "destination");
showText(bus.passengers[0].name, "passenger1");
showText(bus.passengers[1].name, "passenger2");