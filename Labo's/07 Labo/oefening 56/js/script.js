// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Constructor aanmaken voor passagier met behorende properties
function Passenger(name, nationality, age) {
    this.name = name;
    this.nationality = nationality;
    this.age = age;
}

// 3 verschillende passagiers aanmaken
let passenger1 = new Passenger('Nabil', 'Belg', 21);
let passenger2 = new Passenger('Jan', 'Rus', 25);
let passenger3 = new Passenger('Thomas', 'Nederlander', 32);

// In volgende functie maken we een soort van sjabloon waarin we de tekst tonen
function showPassenger(passenger, id) {
    showText(`Naam: ${passenger.name}, Nationaliteit: ${passenger.nationality}, Leeftijd: ${passenger.age} jaar`, id);
}

// De passagiers op scherm tonen
showPassenger(passenger1, "passenger1");
showPassenger(passenger2, "passenger2");
showPassenger(passenger3, "passenger3");