// 2 variabelen aanmaken en waarden toeschrijven
let textHeading = 'Universal function';
let textParagraph = 'To put text on the screen';

// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Oproepen van de functie 
showText(textHeading, "mainHeading");
showText(textParagraph, "paragraph");
