// Variabele aanmaken en waarde toeschrijven
let text = 'Function with parameter!';

//Functie aanmaken die variabele text zal tonen
function HelloWorld(text) {
    return text;
}

//html-element inladen en d.m.v. functie tekst geven
let elMainHeading = document.getElementById("mainHeading");
elMainHeading.textContent = HelloWorld(text);