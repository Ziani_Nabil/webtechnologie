// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Een object bus aanmaken met verschillende properties
let bus = {
    licensePlate: '1-AAA-555',
    destination: 'Antwerpen',
    seats: 52,
    seatsReserved: 43,
    
    CheckAvailability: function() {
    let seatsAvailable = (this.seats - this.seatsReserved); 
    return `Er zijn nog ${seatsAvailable} zetels vrij.`;
    }
}

//De bestemming en het aantal vrije zetels tonen
showText(bus.destination, "destination");
showText(bus.CheckAvailability(), "seatsAvailable");