// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// De template maken voor een object bus
function Bus(licensePlate, destination, seats, seatsReserved) {
    // De verschillende properties, waaronder een array passengers
    this.licensePlate = licensePlate;
    this.destination = destination;
    this.seats = seats;
    this.seatsReserved = seatsReserved;
    this.passengers = new Array();
    // Functie om passagiers toe te voegen
    this.addPassenger = function (name, nationality, age) {
        let newPassenger = new Passenger(name, nationality, age);
        this.passengers.push(newPassenger);
        this.seatsReserved += 1;
    };
    // Functie om het aantal vrije zetels te krijgen
    this.checkAvailability = function () {
        let seatsLeft = (seats - seatsReserved);
        return seatsLeft;
    };
}

// Constructor aanmaken voor passagier met behorende properties
function Passenger(name, nationality, age) {
    this.name = name;
    this.nationality = nationality;
    this.age = age;
}

// Een bus aanmaken met 2 passagiers, de passagiers voegen we toe met functie addPassengers
let bus = new Bus('1-hhh', 'Amsterdam', 45, 25);
bus.addPassenger('Ilias', 'Nederlander', 45);
bus.addPassenger('Nabil', 'Belg', 21);

// Tekst tonen in html-document
showText(bus.destination, "destination");
showText(bus.passengers[0].name, "passenger1");
showText(bus.passengers[1].name, "passenger2");







