//Functie aanmaken die de tekst "Hello function world!" zal tonen
function HelloWorld() {
    let greeting = 'Hello function world!';
    return greeting;
}

//html-element inladen en tekst geven
let elMainHeading = document.getElementById("mainHeading");
elMainHeading.textContent = HelloWorld();