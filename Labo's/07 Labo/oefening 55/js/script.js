// De template maken voor een object bus
function Bus(licensePlate, destination, seats, seatsReserved) {
    // De verschillende properties
    this.licensePlate = licensePlate;
    this.destination = destination;
    this.seats = seats;
    this.seatsReserved = seatsReserved;
    
    this.checkAvailability = function() {
        let seatsLeft = (seats - seatsReserved);
        return seatsLeft;
    };
}

// 2 bussen aanmaken
let bus1 = new Bus('1-AAA-111', 'Nederland', 45, 20);
let bus2 = new Bus('1-ERT-243', 'Budapest', 60, 52);

//De info van bus 1 op scherm tonen
let elInfoBus1 = document.getElementById("infoBus1");
elInfoBus1.textContent = `Bestemming: ${bus1.destination}, Vrije plaatsen: ${bus1.checkAvailability()}, Nummerplaat: ${bus1.licensePlate}`;

//De info van bus 2 op scherm tonen
let elInfoBus2 = document.getElementById("infoBus2");
elInfoBus2.textContent = `Bestemming: ${bus2.destination}, Vrije plaatsen: ${bus2.checkAvailability()}, Nummerplaat: ${bus2.licensePlate}`;