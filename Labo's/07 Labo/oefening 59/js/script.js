// Anonieme functie aanmaken die bepaalde tekst in een gegeven html-element gaat schrijven
let showText = function(text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// De template maken voor een object bus
function Bus(licensePlate, destination, seats, seatsReserved) {
    // De verschillende properties, waaronder een array passengersArray
    this.licensePlate = licensePlate;
    this.destination = destination;
    this.seats = seats;
    this.seatsReserved = seatsReserved;
    this.passengers = new Array();
    // Functie om passagiers toe te voegen
    this.addPassenger = function (name, nationality, age) {
        let newPassenger = new Passenger(name, nationality, age);
        let extraPassenger = this.passengers.push(newPassenger);
        this.seatsReserved += 1;
    };
    // Functie om het aantal vrije zetels te krijgen
    this.checkAvailability = function () {
        let seatsLeft = (seats - seatsReserved);
        return seatsLeft;
    };
}

// Constructor aanmaken voor passagier met behorende properties
 function Passenger(name, nationality, age) {
    this.name = name;
    this.nationality = nationality;
    this.age = age;
}


// Een bus aanmaken met 2 passagiers, de passagiers voegen we toe met functie addPassengers
let bus = new Bus('1-AAA-111', 'Amsterdam', 45, 25);
bus.addPassenger('Ilias', 'Nederlander', 45);
bus.addPassenger('Nabil', 'Belg', 21);

// Nog een bus aanmaken met 2 passagiers
let bus2 = new Bus('1-ABC-123', 'Italië', 75, 62);
bus2.addPassenger('Jan', 'Rus', 33);
bus2.addPassenger('Peter', 'Belg', 25);

// Een array aanmaken met daarin de 2 verschillende bussen
let busArray = [bus, bus2];

// Tekst tonen in html-document
showText(bus.destination, "destination");
showText(bus.passengers[0].name, "passenger1");
showText(bus.passengers[1].name, "passenger2");
showText(bus2.destination, "destination2");
showText(bus2.passengers[0].name, "passenger1_2");
showText(bus2.passengers[1].name, "passenger2_2");