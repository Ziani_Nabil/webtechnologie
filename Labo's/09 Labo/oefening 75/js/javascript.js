//Data arrays en objecten
let openingHourArr = [
    "Maandag 10-16u",
    "Dinsdag 11-17u",
    "Woensdag 8-12u",
    "Donderdag 13-18u",
    "Vrijdag 14-20u",
    "Zaterdag gesloten",
    "Zondag gesloten"
];

let destinationArr = [
    {
        "name": "Budapest",
        "price": 50
    },
    {
        "name": "Parijs",
        "price": 30
    },
    {
        "name": "Praag",
        "price": 40
    },
    {
        "name": "Berlijn",
        "price": 60
    }
];

let busTypes = [
    {
        "type": "Lijnbus",
        "description": "Een lijnbus is een bus die wordt ingezet voor openbaar vervoer. Er kan onderscheid gemaakt worden tussen stads-, streek- en langeafstandsbussen. Lijnbussen zijn vaak voorzien van apparatuur voor verkeerslichtbeïnvloeding, zoals VETAG. "
    },
    {
        "type": "Stadsbus",
        "description": "Een stadsbus is een bus geschikt voor personenvervoer binnen een stad of binnen een stedelijke agglomeratie. Een stadsbus heeft tegenwoordig altijd een automatische transmissie, brede in- en uitstapdeuren en veel staanplaatsen. De nieuwste generatie bussen zijn lagevloerbussen zonder verhoogde instap. De in- en uitstap is dan circa 34 cm vanaf het wegdek. Dit wordt minder bij haltes met verhoogde trottoirs. Sommige bussen beschikken over een knielfunctie. Stadsbussen komen voor in verschillende lengtes: de midibus (9-12 meter), standaard (12 meter), stretched (15 meter, 3 assen waarvan 1 sleepas), de gelede bussen (18 meter, 3 assen) en de dubbelgelede (25,25 meter 4 assen). In sommige steden rijden elektrische stadsbussen zoals trolleybussen. "
    },
    {
        "type": "Streekbus",
        "description": "Een streekbus is een bus geschikt voor vervoer tussen steden en dorpen met meer zitplaatsen dan een stadsbus. De meeste streekbussen zijn 12 meter lang, hoewel ook gelede bussen en bussen met sleepassen worden ingezet. "
    },
    {
        "type": "Langeafstandsbus",
        "description": "Langeafstandsbussen zijn soms 15 meter lang (in plaats van de voor andere bussen gebruikelijke 12 meter), en voorzien van 44 zitplaatsen. Bij de achteras is dan een extra as bijgeplaatst (sleepas) om het draagvermogen per wiel binnen de wettelijke grenzen te houden. Deze zogenaamde derde as welke bij bochten meesturend is, wordt bij een snelheid van 20 km/u of meer weer geblokkeerd om de stabiliteit van de bus bij hogere snelheden te waarborgen. "
    }
]

//Functie om tekst in html te tonen
let showText = function (text, id) {
    let element = document.getElementById(id);
    element.textContent = text;
    return text;
}

// Oefening 70
let loginForm = document.getElementById("loginForm");

function addPersonalMessage(e) {
    e.preventDefault();
    let myForm = loginForm; // Form uit de html halen
    let formData = new FormData(myForm); // De data eruit halen
    let name = formData.get("userName"); // De waarde van name eruit halen
    // Tekst tonen op scherm
    showText(`Dag ${name}, welkom op onze website!`, "welcomeMessage");
}
loginForm.addEventListener("submit", addPersonalMessage);

// Oefening 71
function addListItem(e) {
    for (let i = 0; i < openingHourArr.length; i++) {
        let myList = document.getElementById("openingHoursList"); // Het ul-element ophalen
        let listItem = document.createElement("li"); //Maak een nieuwe list-item aan 
        listItem.textContent = openingHourArr[i]; // Data steken in item
        myList.appendChild(listItem); // De item toevoegen aan de ul-lijst
    }
}
window.addEventListener("load", addListItem);

// Oefening 72

function addDestination(e) {
    let destinationList = document.getElementById("destinationList"); // Het ul-element ophalen
    // Ik ga alle list items in de destinationList ophalen
    let listItemsArr = document.querySelectorAll("#destinationList > li");
    for (let i = 0; i < listItemsArr.length; i++) {
        destinationList.removeChild(listItemsArr[i]);
    }
    // Nu beginnen we met een lege lijst
    for (let i = 0; i < destinationArr.length; i++) {
        // De code wordt pas uitgevoerd als gebruiker klikt op knop "Toon bestemmingen"
        let listItem = document.createElement("li"); //Maak een nieuwe list-item aan 
        listItem.textContent = destinationArr[i].name; // Data steken in item
        destinationList.appendChild(listItem); // De item toevoegen aan de ul-lijst
    }
}
document.getElementById("destinations").addEventListener("click", addDestination);

// Oefening 73

function addBusses(e) {
    let sectionBusses = document.getElementById("busses");
    for (let i = 0; i < busTypes.length; i++) {
        // De drie html-elementen aanmaken
        let articleItem = document.createElement("article");
        let typeItem = document.createElement("h3");
        let descriptionItem = document.createElement("p");
        // De elementen vullen met data
        typeItem.textContent = busTypes[i].type;
        descriptionItem.textContent = busTypes[i].description;
        // De items toevoegen
        articleItem.appendChild(typeItem);
        articleItem.appendChild(descriptionItem);
        sectionBusses.appendChild(articleItem);
    }
}
window.addEventListener("load", addBusses);

// Oefening 74
let settingsForm = document.getElementById("settingsForm");

function addSettings(e) {
    e.preventDefault();
    let formData = new FormData(settingsForm);
    // De kleurwaarde in variabele steken en de pagina-achtergrond aanpassen naar die kleur
    let bgColor = formData.get("backgroundColor");
    document.body.style.backgroundColor = bgColor;
    // Hetzelfde als hierboven, maar dan voor list-style-type (cirkel of vierkant)
    let listStyle = formData.get("listStyleType");
    document.getElementById("openingHoursList").style.listStyleType = listStyle;
    document.getElementById("destinationList").style.listStyleType = listStyle;
}
settingsForm.addEventListener("submit", addSettings);

// Oefening 75
let myTable = document.getElementById("priceTableBody");

function addPrices(e) {
    for (let i = 0; i < destinationArr.length; i++) {
        // 3 html-elementen aanmaken
        let tableRow = document.createElement("tr");
        let tableData1 = document.createElement("td");
        let tableData2 = document.createElement("td");
        // Waarde toekennen aan de 2 kolommen: bestemming en prijs
        tableData1.textContent = destinationArr[i].name;
        tableData2.textContent = destinationArr[i].price;
        // De items toevoegen
        myTable.appendChild(tableRow);
        tableRow.appendChild(tableData1);
        tableRow.appendChild(tableData2);
    }
}
window.addEventListener("load", addPrices);